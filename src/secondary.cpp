#include <iostream>
#include <ncurses.h>
#include <list>
#include <vector>
#include <string>
#include <initializer_list>
#include "primary.h"
#include "dynamicObjects.h"
#include "enemies.h"
#include "secondary.h"
#include "files.h"

using namespace std;

//menu options selection (is part of all menu functions)
int menuChoice(const initializer_list<string> &optionsList)
{
    //paints corners
    wattron(gameWindow, A_REVERSE);
    mvwprintw(gameWindow, 1, 1, "  ");
    mvwprintw(gameWindow, 1, maxx - 3, "  ");
    mvwprintw(gameWindow, maxy - 2, 1, "  ");
    mvwprintw(gameWindow, maxy - 2, maxx - 3, "  ");
    wattroff(gameWindow, A_REVERSE);
    wrefresh(gameWindow);

    //menu implementation
    vector<string> options;
    for (string option : optionsList)
    {
        options.emplace_back(option);
    }

    int choice = 0;

    //print and menu control
    while (1)
    {
        //reprints text
        for (int i = 0; i < (int)options.size(); ++i)
        {
            if (choice == i)
                wattron(gameWindow, A_REVERSE);

            mvwprintw(gameWindow, maxy / 2 + i, maxx / 2 - options[i].size() / 2, options[i].c_str());
            wattroff(gameWindow, A_REVERSE);
        }

        //navigation
        int key = wgetch(gameWindow);
        if (key == KEY_UP)
        {
            choice--;
            if (choice == -1)
                choice = 0;
            continue;
        }
        if (key == KEY_DOWN)
        {
            choice++;
            if (choice == (int)options.size())
                choice = (int)options.size() - 1;
            continue;
        }

        //return choice
        for (int i = 0; i < (int)options.size(); ++i)
        {
            if (key == 10 && choice == i)
                return i;
        }
    }
}

//updates friendly missiles + enemy collisions
void updateFriendlyMisssiles()
{
    for (int i = 0; i < maxx; i++)
    {
        for (auto missile = friendly.missiles[i].begin(); missile != friendly.missiles[i].end(); ++missile)
        {
            //delete if window border is hit
            if ((*missile)->y < 2 || (*missile)->y >= maxy - 2)
            {
                mvwprintw(gameWindow, (*missile)->y, (*missile)->x, " ");
                delete (*missile);
                missile = friendly.missiles[i].erase(missile);
                continue;
            }

            //delete if enemy is hit
            bool stillExists = true;
            for (auto enemy = hostile.enemies[i].begin(); enemy != hostile.enemies[i].end(); ++enemy)
            {
                if ((*missile)->y == (*enemy)->y)
                {
                    //spawn bonus
                    if (rand() % 7 == 0)
                        spawnBonus((*enemy)->y, i);

                    mvwprintw(gameWindow, (*missile)->y, i, " ");
                    delete (*missile);
                    missile = friendly.missiles[i].erase(missile);
                    delete *(enemy);
                    enemy = hostile.enemies[i].erase(enemy);

                    stillExists = false;
                    score += 1;
                    break;
                }
            }

            //update
            if (stillExists == true)
            {
                if ((*missile)->updated == false)
                {
                    int lastX = (*missile)->x;
                    (*missile)->update();
                    int currentX = (*missile)->x;
                    int resultX = currentX - lastX;

                    if (resultX != 0)
                    {
                        if (resultX > 0)
                        {
                            (*missile)->updated = true;
                        }
                        auto cpMissile = missile++;
                        friendly.missiles[i + resultX].splice(friendly.missiles[i + resultX].end(), friendly.missiles[i], cpMissile);
                        --missile;
                    }
                }
                else
                {
                    (*missile)->updated = false;
                }
            }
        }
    }
}

//updates enemy missiles + baricades and players colisions
bool updateHostileMisssiles()
{
    for (auto missile = hostile.missiles.begin(); missile != hostile.missiles.end(); ++missile)
    {
        //delete if window border is hit
        if ((*missile)->y < 2 || (*missile)->y >= maxy - 2)
        {
            mvwprintw(gameWindow, (*missile)->y, (*missile)->x, " ");
            delete (*missile);
            missile = hostile.missiles.erase(missile);
            continue;
        }

        //check collision with baricades
        bool stillExists = true;
        for (auto baricade = friendly.baricades.begin(); baricade != friendly.baricades.end(); ++baricade)
        {
            if ((*missile)->y == baricade->y && ((*missile)->x == baricade->x || (*missile)->x == baricade->x - 1 || (*missile)->x == baricade->x + 1))
            {
                //deletes missile
                mvwprintw(gameWindow, (*missile)->y, (*missile)->x, " ");
                delete (*missile);
                missile = hostile.missiles.erase(missile);
                stillExists = false;

                //deletes baricade
                --baricade->hp;
                if (baricade->hp <= 0)
                {
                    mvwprintw(gameWindow, baricade->y, baricade->x, " ");
                    mvwprintw(gameWindow, baricade->y, baricade->x - 1, " ");
                    mvwprintw(gameWindow, baricade->y, baricade->x + 1, " ");

                    baricade = friendly.baricades.erase(baricade);
                }

                break;
            }
        }

        //game over if player is hit
        if (stillExists == true)
        {
            for (int i = 0; i < (int)friendly.players.size(); ++i)
            {
                if ((*missile)->y == friendly.players[i].y && (*missile)->x == friendly.players[i].x)
                {
                    return true;
                    break;
                }
            }
        }

        if (stillExists == true)
            (*missile)->update();
    }
    return false;
}

//spawns bonus
void spawnBonus(const int y, const int x)
{
    Cbonus bonus = Cbonus(y + 1, x);
    friendly.bonuses.emplace_back(bonus);
}

//updates bonuses + player collisions
void updateBonuses()
{
    for (auto bonus = friendly.bonuses.begin(); bonus != friendly.bonuses.end(); ++bonus)
    {
        bool stillExists = true;
        //deletes bonus and aplies it to player
        for (auto player = friendly.players.begin(); player != friendly.players.end(); ++player)
        {
            if (player->x == bonus->x && player->y == bonus->y)
            {
                player->setBonus();
                bonus = friendly.bonuses.erase(bonus);
                stillExists = false;
                break;
            }
        }

        if (stillExists == true)
            bonus->update();
    }
}

//updates player movement
void updatePlayers()
{
    for (size_t i = 0; i < friendly.players.size(); ++i)
    {
        friendly.players[i].update();
    }
}

//updates turrets
void updateTurrets()
{
    for (auto turret = friendly.turrets.begin(); turret != friendly.turrets.end(); ++turret)
    {
        turret->update();
    }
}

//updates baricades
void updateBaricades()
{
    for (auto baricade = friendly.baricades.begin(); baricade != friendly.baricades.end(); ++baricade)
    {
        baricade->update();
    }
}

//updates enemy action
bool updateEnemies()
{
    for (int i = 0; i < maxx; ++i)
    {
        for (auto enemy = hostile.enemies[i].begin(); enemy != hostile.enemies[i].end(); ++enemy)
        {
            //game over if enemy is at the bottom
            if ((*enemy)->y == maxy - 2)
            {
                return true;
            }

            //update enemy
            if ((*enemy)->updated == false)
            {
                int lastX = (*enemy)->x;
                (*enemy)->update();
                int currentX = (*enemy)->x;
                int resultX = currentX - lastX;

                if (resultX != 0)
                {
                    if (resultX > 0)
                    {
                        (*enemy)->updated = true;
                    }

                    auto cpEnemy = enemy++;
                    hostile.enemies[i + resultX].splice(hostile.enemies[i + resultX].begin(), hostile.enemies[i], cpEnemy);
                    --enemy;
                }
            }
            else
            {
                (*enemy)->updated = false;
            }
        }
    }
    return false;
}

//adds new enemy line at the top of screen if there is space for it
void newEnemyTopLine()
{
    //check if first and second line exists
    bool firstExists = false;
    bool secondExists = false;
    for (int i = 0; i < maxx; ++i)
    {
        for (auto enemy = hostile.enemies[i].begin(); enemy != hostile.enemies[i].end(); ++enemy)
        {
            if ((*enemy)->y == 1)
            {
                firstExists = true;
            }

            if ((*enemy)->y == 2)
            {
                secondExists = true;
            }

            if (firstExists == true && secondExists == true)
                break;
        }

        if (firstExists == true && secondExists == true)
            break;
    }

    //if first and second line does not exist than create first line
    if (firstExists == false && secondExists == false)
    {
        int randomEnemy = rand() % 3;
        int randomSpaces = (rand() % 6) + 3;

        if (randomEnemy == 0)
            newEnemyLine<Cenemy>(1, randomSpaces);
        if (randomEnemy == 1)
            newEnemyLine<Cenemy_dodger>(1, randomSpaces);
        if (randomEnemy == 2)
            newEnemyLine<Cenememy_sharpshooter>(1, randomSpaces);
    }
}

//generates a line of turrets
void newTurretLine(int spaces)
{
    for (int i = maxx / 2; i < maxx - 2; i += spaces)
    {
        Cturret turret = Cturret(maxy - 2, i);
        friendly.turrets.emplace_front(turret);
    }
    for (int i = maxx / 2; i > 1; i -= spaces)
    {
        Cturret turret = Cturret(maxy - 2, i);
        friendly.turrets.emplace_front(turret);
    }
}

//generates a line of baricades
void newBaricadeLine(int spaces)
{
    for (int i = maxx / 2; i < maxx - 2; i += spaces)
    {
        Cbaricade baricade = Cbaricade(maxy - 5, i);
        friendly.baricades.emplace_front(baricade);
    }
    for (int i = maxx / 2; i > 1; i -= spaces)
    {
        Cbaricade baricade = Cbaricade(maxy - 5, i);
        friendly.baricades.emplace_front(baricade);
    }
}

//deletes all
void freeMemory()
{
    //deletes players
    friendly.players.clear();

    //delete turrets
    friendly.turrets.clear();

    //deletes baricades
    friendly.baricades.clear();

    //deletes friendly missiles
    for (int i = 0; i < maxx; i++)
    {
        for (auto it = friendly.missiles[i].begin(); it != friendly.missiles[i].end(); ++it)
        {
            delete *it;
        }
        friendly.missiles[i].clear();
    }

    //deletes enemies
    for (int i = 0; i < maxx; i++)
    {
        for (auto it = hostile.enemies[i].begin(); it != hostile.enemies[i].end(); ++it)
        {
            delete *it;
        }
        hostile.enemies[i].clear();
    }

    //deletes hostile missiles
    for (auto it = hostile.missiles.begin(); it != hostile.missiles.end(); ++it)
    {
        delete *it;
    }
    hostile.missiles.clear();

    //deletes bonuses
    friendly.bonuses.clear();
}

//paints the screen black
void paintTheScreenBlack()
{
    for (int i = 1; i < maxx - 2; ++i)
    {
        for (int j = 1; j < maxy - 1; ++j)
        {
            mvwprintw(gameWindow, j, i, "  ");
        }
    }
}

//prints game over
void gameOverScreen()
{
    //shows game over for 3 seconds
    paintTheScreenBlack();
    mvwprintw(gameWindow, maxy / 2, maxx / 2 - 4, "GAME OVER");
    mvwprintw(gameWindow, maxy / 2 + 1, maxx / 2 - (to_string(score).size() / 2) - 3, "SCORE: %lld", score);
    wrefresh(gameWindow);
    napms(3000);

    //save player to table
    ChighScore::writeHs();

    //shows high scores until user input
    ChighScore::showHs();
}
