#pragma once
#include <list>
#include <vector>
#include "dynamicObjects.h"
#include "staticObjects.h"

/** Height of screen (set in "WINDOW *screen(int &maxy, int &maxx)" function)*/
extern short int maxy;
/** Width of screen (set in "WINDOW *screen(int &maxy, int &maxx)" function)*/
extern short int maxx;
/** Game window for visual output (set in "WINDOW *screen(int &maxy, int &maxx)" function)*/
extern WINDOW *gameWindow;

/** Game time which is used for timing actions of individual objects*/
extern int gameTime;
/** Score, inceremented in game loop and then potentionaly saved to highscore table*/
extern int score;

/** Global container of all friednly objects, e.i. players, missiles, bonuses, turrets, baricades*/
struct TfriendlyObjects
{
    std::vector<Cplayer> players;
    std::vector<std::list<Cmissile *>> missiles;
    std::list<Cbonus> bonuses;
    std::list<Cturret> turrets;
    std::list<Cbaricade> baricades;
};

/** Global container of all hostile objects, e.i. enemies, missiles*/
struct ThostileObjects
{
    std::vector<std::list<Cenemy *>> enemies;
    std::list<Cmissile *> missiles;
};

/** Global container of all friednly objects, e.i. players, missiles, bonuses, turrets, baricades*/
extern TfriendlyObjects friendly;
/** Global container of all hostile objects, e.i. enemies, missiles*/
extern ThostileObjects hostile;

/** Initializes screen and environment in general.
 * @param[out] maxy sets Y size aka height of screen 
 * @param[out] maxx sets X size aka width of screen
 * @return game window
*/
WINDOW *screen(short int &maxy, short int &maxx);

/** Shows main menu*/
void mainMenu();

/** Shows player count menu (after new game in main menu) and lets the player choose number of players*/
void playerCountMenu();

/** Shows map menu (after player count menu) and lets the player choose the map
 * @return true if map loading failed
*/
bool mapMenu();

/** Starts a new game
 * @return true if could not start the game, i.e. map loading failed
*/
bool newGame();

/** Updates the objects (is called every frame)
 * @return true if player is hit and "game over" is triggered
*/
bool update();

/** Basic game loop which updates/creates objects and keeps incrementing time and score*/
void gameLoop();