#include <iostream>
#include <ncurses.h>
#include <list>
#include <vector>
#include "primary.h"
#include "dynamicObjects.h"
#include "staticObjects.h"
#include "missiles.h"

using namespace std;

//=====================================================DYNAMIC OBJECT
CdynamicObject::CdynamicObject(const short int spawny, const short int spawnx) : y(spawny), x(spawnx) {}
CdynamicObject::~CdynamicObject() {}

void CdynamicObject::moveUp(const short int color)
{
    if (y > 1)
    {
        wattron(gameWindow, COLOR_PAIR(0));
        mvwprintw(gameWindow, y, x, " ");
        wattroff(gameWindow, COLOR_PAIR(0));
        y--;
        wattron(gameWindow, COLOR_PAIR(color));
        mvwprintw(gameWindow, y, x, " ");
        wattroff(gameWindow, COLOR_PAIR(color));
    }
}
void CdynamicObject::moveDown(const short int color)
{
    if (y < maxy - 2)
    {
        wattron(gameWindow, COLOR_PAIR(0));
        mvwprintw(gameWindow, y, x, " ");
        wattroff(gameWindow, COLOR_PAIR(0));
        y++;
        wattron(gameWindow, COLOR_PAIR(color));
        mvwprintw(gameWindow, y, x, " ");
        wattroff(gameWindow, COLOR_PAIR(color));
    }
}
void CdynamicObject::moveLeft(const short int color)
{
    if (x > 1)
    {
        wattron(gameWindow, COLOR_PAIR(0));
        mvwprintw(gameWindow, y, x, " ");
        wattroff(gameWindow, COLOR_PAIR(0));
        x--;
        wattron(gameWindow, COLOR_PAIR(color));
        mvwprintw(gameWindow, y, x, " ");
        wattroff(gameWindow, COLOR_PAIR(color));
    }
}
void CdynamicObject::moveRight(const short int color)
{
    if (x < maxx - 2)
    {
        wattron(gameWindow, COLOR_PAIR(0));
        mvwprintw(gameWindow, y, x, " ");
        wattroff(gameWindow, COLOR_PAIR(0));
        x++;
        wattron(gameWindow, COLOR_PAIR(color));
        mvwprintw(gameWindow, y, x, " ");
        wattroff(gameWindow, COLOR_PAIR(color));
    }
}

//=====================================================PLAYER
//spawns player
Cplayer::Cplayer(const short int spawny, const short int spawnx, const short int id) : CdynamicObject(spawny, spawnx), left(true), id(id), bonusNumber(0)
{
    //renders player
    wattron(gameWindow, COLOR_PAIR(1));
    mvwprintw(gameWindow, y, x, " ");
    wattroff(gameWindow, COLOR_PAIR(1));

    //sets start direction
    if (id == 1)
        left = true;
    else if (id == 2)
        left = false;
    else if (id == 3)
        left = true;
    else if (id == 4)
        left = false;
}

void Cplayer::action(const short int key)
{
    Cplayer::shootAndBuild(key, 1, KEY_LEFT, bonusNumber);
    Cplayer::shootAndBuild(key, 2, KEY_RIGHT, bonusNumber);
    Cplayer::shootAndBuild(key, 3, KEY_UP, bonusNumber);
    Cplayer::shootAndBuild(key, 4, KEY_DOWN, bonusNumber);
}
void Cplayer::update()
{
    //if placing turret -paint current position black
    if (bonusNumber == 2)
    {
        wattron(gameWindow, COLOR_PAIR(0));
        mvwprintw(gameWindow, maxy - 2, x, " ");
        wattroff(gameWindow, COLOR_PAIR(0));
    }

    //if placing baricade -paint current position black
    if (bonusNumber == 3)
    {
        wattron(gameWindow, COLOR_PAIR(0));
        //left side
        if (x != 1)
            mvwprintw(gameWindow, y - 2, x - 1, " ");
        //core
        mvwprintw(gameWindow, y - 2, x, " ");
        //right side
        if (x != maxx - 2)
            mvwprintw(gameWindow, y - 2, x + 1, " ");
        wattroff(gameWindow, COLOR_PAIR(0));
    }

    //player move
    Cplayer::move();

    //if placing turret -paint current position cyan
    if (bonusNumber == 2)
    {
        wattron(gameWindow, COLOR_PAIR(3));
        mvwprintw(gameWindow, maxy - 2, x, " ");
        wattroff(gameWindow, COLOR_PAIR(3));
    }

    //if placing baricade -paint current position cyan
    if (bonusNumber == 3)
    {
        wattron(gameWindow, COLOR_PAIR(3));
        //prints left side
        if (x != 1)
            mvwprintw(gameWindow, y - 2, x - 1, " ");
        //prints core
        mvwprintw(gameWindow, y - 2, x, " ");
        //prints right side
        if (x != maxx - 2)
            mvwprintw(gameWindow, y - 2, x + 1, " ");
        wattroff(gameWindow, COLOR_PAIR(3));
    }
}
void Cplayer::setBonus()
{
    if (!(bonusNumber == 2 || bonusNumber == 3))
    {
        int oldBonus = bonusNumber;
        while (oldBonus == bonusNumber)
        {
            bonusNumber = rand() % 7;
            if (bonusNumber == 0)
            {
                ++bonusNumber;
            }
        }
    }
}

//input, id of player, binded key, missile to shoot
void Cplayer::shootAndBuild(const short int key, const short int player, const short int binded, short int &bonusNumber)
{
    if (id == player && key == binded)
    {
        //basic missiles
        if (bonusNumber == 0)
        {
            Cmissile *missile = new Cmissile(y - 1, x);
            friendly.missiles[x].emplace_front(missile);
        }
        //random missiles
        else if (bonusNumber == 1)
        {
            Cmissile_random *missile = new Cmissile_random(y - 1, x);
            friendly.missiles[x].emplace_front(missile);
        }
        //turret
        else if (bonusNumber == 2)
        {
            //change bonus if theres no place left to place turret
            if ((int)friendly.turrets.size() == maxx - 2)
            {
                Cplayer::setBonus();
            }

            //check if turret does not alredy exist at this position
            bool turretAlredyExists = false;
            for (auto turret = friendly.turrets.begin(); turret != friendly.turrets.end(); ++turret)
            {
                if (turret->x == x)
                {
                    turretAlredyExists = true;
                    break;
                }
            }

            //build turrent if doesnt already exist
            if (turretAlredyExists == false)
            {
                Cplayer::buildTurret();
                bonusNumber = 0;
            }
        }
        //baricade
        else if (bonusNumber == 3)
        {
            //check if there is not a baricade already
            int offset = 0;
            for (size_t i = 0; i < friendly.baricades.size(); ++i)
            {
                for (auto baricade = friendly.baricades.begin(); baricade != friendly.baricades.end(); ++baricade)
                {
                    if (((x == baricade->x - 1 || x == baricade->x + 1 || x == baricade->x) && y - 2 - offset == baricade->y))
                    {
                        ++offset;
                        break;
                    }
                }
            }

            //place baricade if not at the end of map
            if (x != maxx - 2 || x != 1)
            {
                Cplayer::buildBaricade(offset);
                bonusNumber = 0;
            }
        }
        //double
        else if (bonusNumber == 4)
        {
            if (x != 1)
            {
                Cmissile *missile = new Cmissile(y - 1, x - 1);
                friendly.missiles[x - 1].emplace_front(missile);
            }

            if (x != maxx - 2)
            {
                Cmissile *missile = new Cmissile(y - 1, x + 1);
                friendly.missiles[x + 1].emplace_front(missile);
            }
        }
        //fast
        else if (bonusNumber == 5)
        {
            Cmissile_fast *missile = new Cmissile_fast(y - 1, x);
            friendly.missiles[x].emplace_front(missile);
        }
        //cone
        else if (bonusNumber == 6)
        {
            //left strafing
            {
                Cmissile_strafing *missile = new Cmissile_strafing(y - 1, x, true);
                friendly.missiles[x].emplace_front(missile);
            }
            //right strafing
            {
                Cmissile_strafing *missile = new Cmissile_strafing(y - 1, x, false);
                friendly.missiles[x].emplace_front(missile);
            }
            //middle
            {
                Cmissile *missile = new Cmissile(y - 1, x);
                friendly.missiles[x].emplace_front(missile);
            }
        }

        //change direction
        left = !left;
    }
}
void Cplayer::buildTurret()
{
    Cturret turret = Cturret(maxy - 2, x);
    friendly.turrets.emplace_front(turret);
}
void Cplayer::buildBaricade(short int offset)
{
    Cbaricade baricade = Cbaricade(y - 2 - offset, x);
    friendly.baricades.emplace_front(baricade);

    //paint the blueprint black
    wattron(gameWindow, COLOR_PAIR(0));
    if (x != 1)
        mvwprintw(gameWindow, y - 2, x - 1, " ");

    mvwprintw(gameWindow, y - 2, x, " ");

    if (x != maxx - 2)
        mvwprintw(gameWindow, y - 2, x + 1, " ");
    wattroff(gameWindow, COLOR_PAIR(0));
}
void Cplayer::move()
{
    if (gameTime % 30 == 0)
    {
        //bounce at the end of map
        if (x == 1 || x == maxx - 2)
        {
            left = !left;
        }

        //bounce from players
        if (friendly.players.size() == 2)
        {
            if (friendly.players[0].x >= friendly.players[1].x - 1)
            {
                friendly.players[0].left = true;
                friendly.players[1].left = false;
            }
        }
        if (friendly.players.size() == 3)
        {
            if (friendly.players[0].x >= friendly.players[2].x - 1)
            {
                friendly.players[0].left = true;
                friendly.players[2].left = false;
            }
            if (friendly.players[2].x >= friendly.players[1].x - 1)
            {
                friendly.players[2].left = true;
                friendly.players[1].left = false;
            }
        }
        if (friendly.players.size() == 4)
        {
            for (int i = 1; i < 4; ++i)
            {
                if (friendly.players[i - 1].x >= friendly.players[i].x - 1)
                {
                    friendly.players[i - 1].left = true;
                    friendly.players[i].left = false;
                }
            }
        }

        //move
        if (left == true)
            moveLeft(1);
        else
            moveRight(1);
    }
}

//=====================================================MISSILE
Cmissile::Cmissile(const short int spawny, const short int spawnx) : CdynamicObject(spawny, spawnx), updated(false) {}

void Cmissile::move()
{
    if (gameTime % 30 == 0)
    {
        moveUp(1);
    }
}

void Cmissile::update()
{
    Cmissile::move();
}

//=====================================================BONUS
Cbonus::Cbonus(const short int spawny, const short int spawnx) : CdynamicObject(spawny, spawnx) {}

void Cbonus::move()
{
    if (gameTime % 10 == 0)
        moveDown(3);
}

void Cbonus::update()
{
    if (y != maxy - 3)
    {
        Cbonus::move();
    }
    else
    {
        wattron(gameWindow, COLOR_PAIR(3));
        mvwprintw(gameWindow, y, x, " ");
        wattroff(gameWindow, COLOR_PAIR(3));
    }
}

//=====================================================ENEMY
Cenemy::Cenemy(const short int spawny, const short int spawnx) : CdynamicObject(spawny, spawnx), updated(false) {}

void Cenemy::move()
{
    if (gameTime % 500 == 0)
    {
        moveDown(1);
    }
}

void Cenemy::update()
{
    Cenemy::move();
    Cenemy::printItself();
}

void Cenemy::printItself()
{
    wattron(gameWindow, COLOR_PAIR(1));
    mvwprintw(gameWindow, y, x, " ");
    wattroff(gameWindow, COLOR_PAIR(1));
}