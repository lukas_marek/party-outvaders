#pragma once

/** High score table*/
class ChighScore
{
public:
	/** Reads high score table from a file and then modifies it according to user input*/
	static void writeHs();

	/** Reads high score table from file and prints it on the screen*/
	static void showHs();

private:
	/** User input function used in "writeHs()"
	 * @return player's name
	 */
	static std::string inputNameScreen();

	/** Max lenght of the players/team name*/
	static const short unsigned int maxNameLenght = 14;
};

/** Loads map from a file
 * @param[in] mapName name of the map i.e. file
 * @return true if file does not exists or cannot be opened
 */
bool loadMap(std::string mapName);

/** Checks if the file can be openned
 * @param[in] file stream to be opened
 * @return true if file does not exists or cannot be opened
 */
bool failedToOpenFile(std::ifstream &file);