#pragma once

/** Menu options selection (is part part of all menu functions)
 * @param[in] optionsList Initilizer list of options to be selected
 * @return number from 0 up to maximum number of options
*/
int menuChoice(const std::initializer_list<std::string> &optionsList);

/** updates friendly missiles*/
void updateFriendlyMisssiles();

/** Updates enemy missiles
 * @return true if player is hit and "game over" is triggered
*/
bool updateHostileMisssiles();

/** Spawns bonus
 * @param[in] y Y spawn coordinate
 * @param[in] x X spawn coordinate
*/
void spawnBonus(const int y, const int x);

/** Updates bonuses*/
void updateBonuses();

/** Updates player movement*/
void updatePlayers();

/** Updates turrets*/
void updateTurrets();

/** Updates baricades*/
void updateBaricades();

/** Updates enemy action
 * @return true if some enemy reached the end of screen
*/
bool updateEnemies();

/**Adds new enemy line at the top of screen if there is space for it*/
void newEnemyTopLine();

/** Deletes all abjects and frees the memory*/
void freeMemory();

/** Generates a new line of enemies of given type
 * @param[in] row Row, i.e. position/height where to spawn
 * @param[in] spaces Spaces between individual enemies
*/
template <typename type>
void newEnemyLine(int row, int spaces)
{
    for (int i = maxx / 2; i < maxx - 2; i += spaces)
    {
        Cenemy *enemy = new type(row, i);
        hostile.enemies[i].emplace_front(enemy);
    }
    for (int i = maxx / 2; i > 1; i -= spaces)
    {
        Cenemy *enemy = new type(row, i);
        hostile.enemies[i].emplace_front(enemy);
    }
}

/** Generates a line of turrets*/
void newTurretLine(int spaces);

/** Generates a line of baricades*/
void newBaricadeLine(int spaces);

/** Paints the screen black*/
void paintTheScreenBlack();

/** Prints game over*/
void gameOverScreen();
