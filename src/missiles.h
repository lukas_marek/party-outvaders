#pragma once

/** Missile moving in random direction except down*/
class Cmissile_random : public Cmissile
{
public:
    /** Constructor of a missile moving in random direction
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cmissile_random(const short int spawny, const short int spawnx);

    /** Updates randomly moving missile in every frame*/
    void update() override;

protected:
    /** Defines the exact movement of the randomly moving missile*/
    void move() override;
};

/** Missile moving down against players*/
class Cmissile_enemy : public Cmissile
{
public:
    /** Constructor of an enemy missile
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cmissile_enemy(const short int spawny, const short int spawnx);

    /** Updates enemy missile in every frame*/
    void update() override;

protected:
    /** Defines the exact movement of the enemy missile*/
    void move() override;
};

/** Missile moving much faster than basic missile*/
class Cmissile_fast : public Cmissile
{
public:
    /** Constructor of a fast missile
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cmissile_fast(const short int spawny, const short int spawnx);

    /** Updates fast missile in every frame*/
    void update() override;

protected:
    /** Defines the exact movement of the fast missile*/
    void move() override;
};

/** Missile strafing to the side*/
class Cmissile_strafing : public Cmissile
{
public:
    /** Constructor of a strafing missile
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     * @param[in] left decides whenever the missile should strafe left or right
     */
    Cmissile_strafing(const short int spawny, const short int spawnx, const bool left);

    /** Updates strafing missile in every frame*/
    void update() override;

protected:
    /** If equal to zero the missile will move to the side else if equal to 1 or 2 the missile will move up*/
    short int moveToSide;
    /** Decides whenever the missile should strafe left or right*/
    bool left;

    /** Defines the exact movement of the strafing missile*/
    void move() override;
};