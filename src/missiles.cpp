#include <iostream>
#include <ncurses.h>
#include <list>
#include <vector>
#include "primary.h"
#include "dynamicObjects.h"
#include "missiles.h"

//------------------------------------random
Cmissile_random::Cmissile_random(const short int spawny, const short int spawnx) : Cmissile(spawny, spawnx) {}

void Cmissile_random::move()
{
    if (gameTime % 30 == 0)
    {
        int randomDirection = rand() % 3;
        if (randomDirection == 0)
            moveUp(1);
        if (randomDirection == 1)
            moveLeft(1);
        if (randomDirection == 2)
            moveRight(1);
    }
}

void Cmissile_random::update()
{
    Cmissile_random::move();
}

//------------------------------------enemy (missile)
Cmissile_enemy::Cmissile_enemy(const short int spawny, const short int spawnx) : Cmissile(spawny, spawnx) {}

void Cmissile_enemy::update()
{
    Cmissile_enemy::move();
}

void Cmissile_enemy::move()
{
    if (gameTime % 75 == 0)
    {
        moveDown(2);
    }
}

//------------------------------------fast
Cmissile_fast::Cmissile_fast(const short int spawny, const short int spawnx) : Cmissile(spawny, spawnx) {}

void Cmissile_fast::update()
{
    Cmissile_fast::move();
}

void Cmissile_fast::move()
{
    if (gameTime % 5 == 0)
    {
        moveUp(1);
    }
}

//------------------------------------strafing
Cmissile_strafing::Cmissile_strafing(const short int spawny, const short int spawnx, const bool left) : Cmissile(spawny, spawnx), moveToSide(0), left(left) {}

void Cmissile_strafing::update()
{
    Cmissile_strafing::move();
}

void Cmissile_strafing::move()
{
    if (gameTime % 20 == 0)
    {
        if (x == 1 || x == maxx - 2)
        {
            left = !left;
        }

        if (moveToSide == 0)
        {
            if (left == true)
                moveLeft(1);
            else
                moveRight(1);

            ++moveToSide;
        }
        else if (moveToSide == 1)
        {
            moveUp(1);
            ++moveToSide;
        }
        else if (moveToSide == 2)
        {
            moveUp(1);
            moveToSide = 0;
        }
    }
}
