#include <iostream>
#include <ncurses.h>
#include "primary.h"

using namespace std;

int main()
{
    //creates game window
    gameWindow = screen(maxy, maxx);

    //checks if the game window is big enough
    if (maxy < 20 || maxx < 25)
    {
        delwin(gameWindow);
        delwin(stdscr);
        endwin();
        system("echo screen_is_too_small");
        return 1;
    }

    //starts the game
    mainMenu();

    //dealocates
    delwin(gameWindow);
    delwin(stdscr);
    endwin();

    return 0;
}