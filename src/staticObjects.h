#pragma once

/** Base class for all static (non movable) objects.*/
class CstaticObject
{
public:
    CstaticObject();
    /** Constructor of generic static object
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    CstaticObject(const short int spawnY, const short int spawnX);

    /** Basic virtual destructor of static objects*/
    virtual ~CstaticObject();

    /** Y position on the screen*/
    short int y;
    /** X position on the screen*/
    short int x;

    /** Updates static obejct in every frame*/
    virtual void update() = 0;

protected:
    /** Prints itself in every frame, thus preventing temporary blackouts from missiles passing through*/
    virtual void printIself() = 0;
};

/** Automaticaly shooting placeble turret*/
class Cturret : public CstaticObject
{
public:
    /** Constructor of turret
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cturret(const short int spawnY, const short int spawnX);

    /** Updates turret in every frame*/
    void update() override;

protected:
    /** Spawns a missile*/
    void shoot();

    /** Prints itself in every frame, thus preventing temporary blackouts from missiles passing through*/
    void printIself() override;
};

/** Barricade shielding players from enemy missiles*/
class Cbaricade : public CstaticObject
{
public:
    /** Constructor of baricade
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cbaricade(const short int spawnY, const short int spawnX);

    /** Updates baricade in every frame*/
    void update() override;
    
    /** Number of missiles it can hit before it's destroyed*/
    short int hp;

protected:
    /** Prints itself in every frame, thus preventing temporary blackouts from missiles passing through*/
    void printIself() override;
};
