#pragma once

/** Dodging enemy*/
class Cenemy_dodger : public Cenemy
{
public:
    /** Constructor of a dodging enemy
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cenemy_dodger(const short int spawny, const short int spawnx);

    /** Updates dodging enemy in every frame*/
    void update() override;

protected:
    /** Defines the exact movement of the dodging enemy*/
    void move() override;

    /** Decides if the enemy should move left in this frame*/
    bool left;
    /** Decides if the enemy should move right in this frame*/
    bool right;
    /** Decides if the enemy should move down in this frame*/
    bool down;
    /** Decides if the enemy should move down again or not in this frame*/
    bool down1;
};

/** Shooting enemy*/
class Cenememy_sharpshooter : public Cenemy
{
public:
    /** Constructor of a shooting enemy
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cenememy_sharpshooter(const short int spawny, const short int spawnx);

    /** Updates shooting enemy in every frame*/
    void update() override;

protected:
    /** Spawns a hostile missile moving downwards against players*/
    void shoot();
};