#include <iostream>
#include <ncurses.h>
#include <list>
#include "primary.h"
#include "dynamicObjects.h"
#include "staticObjects.h"

using namespace std;

//=====================================================STATIC OBJECT
CstaticObject::CstaticObject() {}

CstaticObject::CstaticObject(const short int spawnY, const short int spawnX) : y(spawnY), x(spawnX) {}

CstaticObject::~CstaticObject() {}

//=====================================================TURRET
Cturret::Cturret(const short int spawnY, const short int spawnX) : CstaticObject(spawnY, spawnX)
{
    Cturret::printIself();
}

void Cturret::update()
{
    Cturret::shoot();
    Cturret::printIself();
}

void Cturret::printIself()
{
    wattron(gameWindow, COLOR_PAIR(1));
    mvwprintw(gameWindow, y, x, " ");
    wattroff(gameWindow, COLOR_PAIR(1));
}

void Cturret::shoot()
{
    if (rand() % 3000 == 0)
    {
        Cmissile *missile = new Cmissile(y - 1, x);
        friendly.missiles[x].emplace_front(missile);
    }
}

//=====================================================BARICADE
Cbaricade::Cbaricade(const int short spawnY, const int short spawnX) : CstaticObject(spawnY, spawnX), hp(3)
{
    Cbaricade::printIself();
}

void Cbaricade::update()
{
    Cbaricade::printIself();
}

void Cbaricade::printIself()
{
    wattron(gameWindow, COLOR_PAIR(1));

    //prints left side
    if (x != 1)
        mvwprintw(gameWindow, y, x - 1, " ");
    //prints core
    mvwprintw(gameWindow, y, x, " ");
    //prints right side
    if (x != maxx - 2)
        mvwprintw(gameWindow, y, x + 1, " ");

    wattroff(gameWindow, COLOR_PAIR(1));
}