#include <iostream>
#include <ncurses.h>
#include <list>
#include <vector>
#include <cstdlib>
#include <ctime>
#include "primary.h"
#include "dynamicObjects.h"
#include "enemies.h"
#include "secondary.h"
#include "files.h"

using namespace std;

//creates game window
short int maxy = 0;
short int maxx = 0;
WINDOW *gameWindow = NULL;
WINDOW *menuWindow = NULL;

//initiates time
int gameTime = 0;
int score = 0;

TfriendlyObjects friendly;
ThostileObjects hostile;

//game window
WINDOW *screen(short int &maxy, short int &maxx)
{
    //init screen
    initscr();

    //disables cursor
    curs_set(0);

    //gets max size of screen
    int scrMaxy = 0;
    int scrMaxx = 0;
    getmaxyx(stdscr, scrMaxy, scrMaxx);
    //turns of echo
    noecho();
    //enables ctrl+c
    cbreak();
    //enables colors
    start_color();

    //makes color pair for future use
    init_pair(1, COLOR_WHITE, COLOR_WHITE);
    init_pair(2, COLOR_RED, COLOR_RED);
    init_pair(3, COLOR_CYAN, COLOR_CYAN);
    init_pair(4, COLOR_MAGENTA, COLOR_MAGENTA);
    init_pair(5, COLOR_GREEN, COLOR_GREEN);

    //makes new window
    gameWindow = newwin(scrMaxy, scrMaxx, 0, 0);

    //refreshes screen
    refresh();
    //gets size of window
    getmaxyx(gameWindow, maxy, maxx);
    //enables arrow input
    keypad(gameWindow, true);

    //frames the window
    box(gameWindow, 0, 0);

    //sets nodelay
    nodelay(gameWindow, true);

    friendly.missiles.resize(maxx);
    hostile.enemies.resize(maxx);

    return gameWindow;
}

//main menu
void mainMenu()
{
    //paints the screen black
    paintTheScreenBlack();

    if (maxx >= 70)
    {
        string p0 = " ___          _           ___       _               _            ";
        string p1 = "| _ \\__ _ _ _| |_ _  _   / _ \\ _  _| |___ ____ _ __| |___ _ _ ___";
        string p2 = "|  _/ _` | '_|  _| || | | (_) | || |  _\\ V / _` / _` / -_) '_(_-<";
        string p3 = "|_| \\__,_|_|  \\__|\\_, |  \\___/ \\_,_|\\__|\\_/\\__,_\\__,_\\___|_| /__/";
        string p4 = "                  |__/                                           ";

        mvwprintw(gameWindow, maxy / 2 - 7, maxx / 2 - 33, "%s", p0.c_str());
        mvwprintw(gameWindow, maxy / 2 - 6, maxx / 2 - 33, "%s", p1.c_str());
        mvwprintw(gameWindow, maxy / 2 - 5, maxx / 2 - 33, "%s", p2.c_str());
        mvwprintw(gameWindow, maxy / 2 - 4, maxx / 2 - 33, "%s", p3.c_str());
        mvwprintw(gameWindow, maxy / 2 - 3, maxx / 2 - 33, "%s", p4.c_str());
    }
    else
    {
        mvwprintw(gameWindow, maxy / 2 - 2, maxx / 2 - 7, "PARTY OUTVADERS");
    }

    //handles menu navigation
    int choice = menuChoice({"new game", "scores", "exit"});

    //starts game
    if (choice == 0)
    {
        if (newGame() == true)
        {
            mainMenu();
        }
    }

    //shows high scores
    else if (choice == 1)
    {
        ChighScore::showHs();

        mainMenu();
    }

    //quits
    else if (choice == 2)
    {
        return;
    }
}

//player count menu (after new game in main menu)
void playerCountMenu()
{
    //paints the screen black
    paintTheScreenBlack();

    //handles menu navigation
    int choice = menuChoice({"1 player(s)", "2 player(s)", "3 player(s)", "4 player(s)"});

    //starts the game with 1 player
    if (choice == 0)
    {
        //first
        Cplayer player1(maxy - 3, (maxx / 2 - 2), 1);
        srand(time(NULL));
        if (rand() % 2 == 0)
            player1.left = false;
        friendly.players.emplace_back(player1);

        return;
    }

    //starts the game with 2 players
    else if (choice == 1)
    {
        //first
        Cplayer player1(maxy - 3, (maxx / 2 - 2), 1);
        friendly.players.emplace_back(player1);
        //second
        Cplayer player2(maxy - 3, (maxx / 2 + 2), 2);
        player2.left = false;
        friendly.players.emplace_back(player2);

        return;
    }

    //starts the game with 3 players
    else if (choice == 2)
    {
        //first
        Cplayer player1(maxy - 3, (maxx / 3), 1);
        friendly.players.emplace_back(player1);
        //second
        Cplayer player2(maxy - 3, (maxx - (maxx / 3)), 2);
        friendly.players.emplace_back(player2);
        //third
        Cplayer player3(maxy - 3, (maxx / 2), 3);
        srand(time(NULL));
        if (rand() % 2 == 0)
            player3.left = false;
        friendly.players.emplace_back(player3);

        return;
    }

    //starts the game with 4 players
    else if (choice == 3)
    {
        //first
        Cplayer player1(maxy - 3, (maxx / 4), 1);
        friendly.players.emplace_back(player1);
        //second
        Cplayer player2(maxy - 3, (maxx / 2) - 1, 2);
        player2.left = true;
        friendly.players.emplace_back(player2);
        //third
        Cplayer player3(maxy - 3, (maxx / 2) + 1, 3);
        player3.left = false;
        friendly.players.emplace_back(player3);
        //fourth
        Cplayer player4(maxy - 3, (maxx - maxx / 4), 4);
        friendly.players.emplace_back(player4);

        return;
    }
}

//map menu (after player count menu)
bool mapMenu()
{
    //paints the screen black
    paintTheScreenBlack();

    //handles menu navigation
    int choice = menuChoice({"easy", "hard", "balanced"});

    //loads ez map
    if (choice == 0)
    {
        if (loadMap("easy") == true)
            return true;
    }

    //loads hard map
    else if (choice == 1)
    {
        if (loadMap("hard") == true)
            return true;
    }

    //loads balanced map
    else if (choice == 2)
    {
        if (loadMap("balanced") == true)
            return true;
    }
    return false;
}

//creates ininitial objects
bool newGame()
{
    //select number of players
    playerCountMenu();

    //select map
    if (mapMenu() == true)
        return true;

    //paints the sceen black
    paintTheScreenBlack();

    //resets gametime and score
    gameTime = 0;
    score = 0;

    //starts game loop
    gameLoop();

    return false;
}

//updates object, returns true if game over
bool update()
{
    //add score based on time survived
    if (gameTime % 1000 == 0)
        score += 1;

    //updates enemy missiles, return true if player hit
    if (updateHostileMisssiles() == true)
        return true;

    //updates enemy action
    if (updateEnemies() == true)
        return true;

    //updates friendly missiles
    updateFriendlyMisssiles();

    //update bonuses
    updateBonuses();

    //updates player movement
    updatePlayers();

    //updates turrets
    updateTurrets();

    //updates baricades
    updateBaricades();

    //adds new enemy line at the top of screen if there is space for it
    newEnemyTopLine();

    return false;
}

//loops through game
void gameLoop()
{
    while (1)
    {
        //updates all objects and detects whenever player is hit
        if (update() == true)
        {
            freeMemory();

            gameOverScreen();

            mainMenu();
            break;
        }

        //input + window refresh
        int key = wgetch(gameWindow);

        //esc
        if (key == 27)
        {
            freeMemory();
            mainMenu();

            break;
        }

        //players action
        for (auto it = friendly.players.begin(); it != friendly.players.end(); it++)
        {
            it->action(key);
        }

        //controlls time
        if (gameTime == 10000000)
            gameTime = 0;

        ++gameTime;

        napms(1);
    }
}
