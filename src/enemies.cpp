#include <iostream>
#include <ncurses.h>
#include <list>
#include <vector>
#include "primary.h"
#include "dynamicObjects.h"
#include "staticObjects.h"
#include "missiles.h"
#include "enemies.h"

//------------------------------------dodger
Cenemy_dodger::Cenemy_dodger(const short int spawny, const short int spawnx) : Cenemy(spawny, spawnx), left(true), right(true), down(false), down1(false) {}

void Cenemy_dodger::move()
{
    if (gameTime % 250 == 0)
    {
        if (left == true)
        {
            moveLeft(1);
            moveLeft(1);
            left = false;
            down = true;
        }
        else if (right == true)
        {
            moveRight(1);
            moveRight(1);
            right = false;
            down1 = true;
        }
    }
    if (gameTime % 500 == 0)
    {
        if (down == true)
        {
            moveDown(1);
            down = false;
            right = true;
        }

        else if (down1 == true)
        {
            moveDown(1);
            down1 = false;
            left = true;
        }
    }
}

void Cenemy_dodger::update()
{
    Cenemy_dodger::move();
    Cenemy::printItself();
}

//------------------------------------sharpshooter
Cenememy_sharpshooter::Cenememy_sharpshooter(const short int spawny, const short int spawnx) : Cenemy(spawny, spawnx) {}

void Cenememy_sharpshooter::shoot()
{
    if (gameTime % 50 == 0)
    {
        int chanceToShoot = rand() % (hostile.missiles.size() + 500);
        if (chanceToShoot == 0)
        {
            //y+1 offsets the rocket down
            Cmissile_enemy *missile = new Cmissile_enemy(y + 1, x);
            hostile.missiles.emplace_front(missile);
        }
    }
}

void Cenememy_sharpshooter::update()
{
    Cenememy_sharpshooter::move();
    Cenemy::printItself();
    Cenememy_sharpshooter::shoot();
}
