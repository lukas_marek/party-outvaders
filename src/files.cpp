#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "ncurses.h"
#include <algorithm>
#include "files.h"
#include "primary.h"
#include "secondary.h"
#include "enemies.h"

using namespace std;

void ChighScore::writeHs()
{
    //vector of scores
    vector<pair<string, int>> table;

    //loads data into vector
    ifstream itable("src/highScore");

    {
        string name;
        string score;
        short int i = 0;
        while (itable >> name >> score)
        {
            if (i == 10)
                break;

            if (score.size() <= 9 && name.size() <= maxNameLenght)
                table.emplace_back(make_pair(name, stoi(score)));

            ++i;
        }
    }

    itable.close();

    //opens name screen and adds new score
    table.emplace_back(make_pair(inputNameScreen(), score));

    //sorts scores
    stable_sort(table.rbegin(), table.rend(), [](const pair<string, long long int> &a, const pair<string, long long int> &b) { return (a.second < b.second); });

    //writes back to file
    ofstream otable("src/highScore");
    {
        short int i = 0;
        for (auto it = table.begin(); it != table.end(); ++it)
        {
            if (i == 10)
                break;

            otable << it->first << " " << it->second << endl;
            ++i;
        }
    }

    otable.close();
}

void ChighScore::showHs()
{
    paintTheScreenBlack();

    ifstream itable("src/highScore");

    //prints title
    mvwprintw(gameWindow, maxy / 2 - 5, maxx / 2 - 5, "HIGH SCORES");

    //prints names and scores
    {
        string name;
        string score;
        short int i = 0;
        while (itable >> name >> score)
        {
            if (i == 10)
                break;

            //marks cheater
            bool scoreCheater = false;
            bool nameCheater = false;

            if (score.size() > 9 || stoi(score) < 0)
                scoreCheater = true;
            else if (score.size() <= 9)
            {
                for (size_t i = 0; i < score.size(); ++i)
                {
                    if (score[i] < '0' || score[i] > '9')
                    {
                        scoreCheater = true;
                        break;
                    }
                }
            }

            if (name.size() > maxNameLenght)
                nameCheater = true;

            if (nameCheater == false && scoreCheater == true)
                mvwprintw(gameWindow, maxy / 2 + i - 3, maxx / 2 - ((name.size() + 1 + 9) / 2), "%s CORRUPTED", name.c_str());
            else if (nameCheater == true && scoreCheater == false)
                mvwprintw(gameWindow, maxy / 2 + i - 3, maxx / 2 - (9 / 2), "CORRUPTED");
            else if (nameCheater == true && scoreCheater == true)
                mvwprintw(gameWindow, maxy / 2 + i - 3, maxx / 2 - (9 / 2), "CORRUPTED");
            else if (stoi(score) != 0)
                mvwprintw(gameWindow, maxy / 2 + i - 3, maxx / 2 - ((name.size() + 1 + score.size()) / 2), "%s %s", name.c_str(), score.c_str());

            ++i;
        }
    }
    box(gameWindow, 0, 0);

    itable.close();

    //waits for user input (space)
    {
        short int time = 0;
        while (wgetch(gameWindow) != 32)
        {
            if (time == 500)
            {
                wattron(gameWindow, A_REVERSE);
                mvwprintw(gameWindow, maxy / 2 + 8, maxx / 2 - 9, "press space to exit");
                wattroff(gameWindow, A_REVERSE);
                wrefresh(gameWindow);
            }
            if (time == 1000)
            {
                mvwprintw(gameWindow, maxy / 2 + 8, maxx / 2 - 9, "                   ");
                wrefresh(gameWindow);
                time = 0;
            }

            time++;
            napms(1);
        }
    }
}

string ChighScore::inputNameScreen()
{
    //user input screen
    paintTheScreenBlack();
    mvwprintw(gameWindow, maxy / 2 - 1, maxx / 2 - 12, "enter your's/team's name:");

    //new name input
    string name;
    move(maxy / 2, maxx / 2);
    curs_set(2);

    short int letter = wgetch(gameWindow);
    while (1)
    {
        letter = getch();

        //if enter is hit and size is not 0 then save it
        if (letter == '\n' && name.size() != 0)
        {
            break;
        }

        //accept only some basic characters
        if (letter < 33 || letter > 127)
        {
            if (letter == 32)
            {
                continue;
            }
            for (short int i = 1; i < maxx - 2; i++)
            {
                mvwprintw(gameWindow, maxy / 2, i, " ");
            }
            mvwprintw(gameWindow, maxy / 2, (maxx / 2) - (name.size() / 2), "%s", name.c_str());
            wrefresh(gameWindow);
            continue;
        }

        //if backspace is pressed then delete character
        if (letter == 127)
        {
            if (name.size() != 0)
                name.pop_back();

            for (unsigned short int i = (maxx / 2) - (name.size() / 2) - 2; i <= (maxx / 2) + (name.size() / 2); i++)
            {
                mvwprintw(gameWindow, maxy / 2, i, " ");
            }
            mvwprintw(gameWindow, maxy / 2, (maxx / 2) - (name.size() / 2), "%s", name.c_str());
            wrefresh(gameWindow);
            continue;
        }

        //do not accept names over given size characters
        if (name.size() != maxNameLenght)
        {
            name.push_back(letter);

            mvwprintw(gameWindow, maxy / 2, (maxx / 2) - (name.size() / 2), "%s", name.c_str());
            wrefresh(gameWindow);
        }

        //moves cursor at the end
        if (name.size() % 2 != 0)
            move(maxy / 2, (maxx / 2) + (name.size() / 2) + 1);
        else
            move(maxy / 2, (maxx / 2) + (name.size() / 2));
    }
    //turns of cursor
    curs_set(0);

    return name;
}

bool loadMap(const string mapName)
{
    ifstream map("src/" + mapName);

    //failed to open file
    if (failedToOpenFile(map) == true)
        return true;

    //read data
    {
        string object;
        short int row;
        short int spaces;
        while (map >> object >> row >> spaces)
        {
            if (object == "basic")
            {
                newEnemyLine<Cenemy>(row, spaces);
            }
            else if (object == "dodger")
            {
                newEnemyLine<Cenemy_dodger>(row, spaces);
            }
            else if (object == "sharpshooter")
            {
                newEnemyLine<Cenememy_sharpshooter>(row, spaces);
            }
            else if (object == "turret")
            {
                newTurretLine(spaces);
            }
            else if (object == "baricade")
            {
                newBaricadeLine(spaces);
            }
        }
    }

    map.close();
    return false;
}

bool failedToOpenFile(ifstream &file)
{
    if (file.is_open() == false)
    {
        paintTheScreenBlack();
        mvwprintw(gameWindow, maxy / 2 - 1, maxx / 2 - 2, "ERROR");
        mvwprintw(gameWindow, maxy / 2, maxx / 2 - 9, "file does not exist");
        wrefresh(gameWindow);
        freeMemory();
        napms(2000);
        return true;
    }
    return false;
}