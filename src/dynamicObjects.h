#pragma once

/** Base class for all dynamic (movable) objects*/
class CdynamicObject
{
public:
    /** Constructor of generic dynamic object
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    CdynamicObject(const short int spawny, const short int spawnx);

    /** Basic virtual destructor of dynamic objects*/
    virtual ~CdynamicObject();

    /** Y position on the screen*/
    short int y;
    /** X position on the screen*/
    short int x;

    /** Update which is called on every frame, usually consists of move() and sometimes shoot(), drawItself() etc.*/
    virtual void update() = 0;

protected:
    /** Defines how exactly does the object move*/
    virtual void move() = 0;

    /** Moves the object up on the screen
     * @param[in] color number of "COLOR_PAIR" (0 = black, 1 = white, 2 = red, 3 = cyan, 4 = magenta, 5 = green)
     */
    void moveUp(const short int color);

    /** Moves the object down on the screen
     * @param[in] color number of "COLOR_PAIR" (0 = black, 1 = white, 2 = red, 3 = cyan, 4 = magenta, 5 = green)
     */

    void moveDown(const short int color);
    /** Moves the object left on the screen
     * @param[in] color number of "COLOR_PAIR" (0 = black, 1 = white, 2 = red, 3 = cyan, 4 = magenta, 5 = green)
     */

    void moveLeft(const short int color);

    /** Moves the object right on the screen
     * @param[in] color number of "COLOR_PAIR" (0 = black, 1 = white, 2 = red, 3 = cyan, 4 = magenta, 5 = green)
     */
    void moveRight(const short int color);
};

/** Player*/
class Cplayer : public CdynamicObject
{
public:
    /** Constructor of player object
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     * @param[in] id id or also "player number"
     */
    Cplayer(const short int spawny, const short int spawnx, const short int id);

    /** Action which happens immediatly after pressing a button on keyboard
     * @param[in] key decimal ascii number of key
     */
    void action(const short int key);

    /** Sets number of bonus after colliding with one and affects the action of player i.e. switches weapons or builds turret/baricade*/
    void setBonus();

    /** Updates player in every frame*/
    void update() override;

    /** Decides whenever the player is moving left (true) or moving right (false)*/
    bool left;

private:
    /** id id or also "player number"*/
    short int id;

    /** Number of bonus which affects shooting and building*/
    short int bonusNumber;

    /** Lets the player shoots or build
     * @param[in] key key pressed on keyboard
     * @param[in] player id or "player number", used for deciding which player should make an action
     * @param[in] binded binded key, if binded key and pressed key are the same then the action will be performed
     * @param[in,out] bonusNumber decides with which weapon to shoot or which object to build
     */
    void shootAndBuild(const short int key, const short int player, const short int binded, short int &bonusNumber);

    /** Builds a turret*/
    void buildTurret();

    /** Builds a baricade*/
    void buildBaricade(short int offset);

    /** Defines the exact movement of the player*/
    void move() override;
};

/** Basic missile and also a base class for all other missiles*/
class Cmissile : public CdynamicObject
{
public:
    /** Constructor of a missile
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cmissile(const short int spawny, const short int spawnx);

    /** Updates missile in every frame*/
    virtual void update() override;

    /** Prevents from updating missile multiple times in one frame*/
    bool updated;

protected:
    /** Defines the exact movement of the missile*/
    virtual void move() override;
};

/** Bonus*/
class Cbonus : public CdynamicObject
{
public:
    /** Constructor of a bonus
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cbonus(const short int spawny, const short int spawnx);

    /** Updates bonus in every frame*/
    virtual void update() override;

protected:
    /** Defines the exact movement of the bonus*/
    virtual void move() override;
};

/** Basic enemy and also a base class for all other enemies*/
class Cenemy : public CdynamicObject
{
public:
    /** Constructor of an enemy
     * @param[in] spawny Y spawn location 
     * @param[in] spawnx X spawn location
     */
    Cenemy(const short int spawny, const short int spawnx);

    /** Updates enemy in every frame*/
    virtual void update() override;

    /** Prevents from updating enemy multiple times in one frame*/
    bool updated;

protected:
    /** Defines the exact of the bonus*/
    virtual void move() override;

    /** Draws itself in every frame, thus preventing temporary blackouts from missiles passing through*/
    void printItself();
};
