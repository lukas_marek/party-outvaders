#Author: Lukáš Marek
#Project: Party Outvaders

gameName = partyOutvaders
buildDir = src/build
flags = -Wall -pedantic -Wextra -std=c++14 -g
objectFiles = $(buildDir)/main.o $(buildDir)/primary.o $(buildDir)/secondary.o $(buildDir)/dynamicObjects.o $(buildDir)/enemies.o $(buildDir)/missiles.o $(buildDir)/staticObjects.o $(buildDir)/files.o
files = highscore

all: compile doc

compile: $(objectFiles)
	g++ $(flags) $(objectFiles) -o $(gameName) -lncurses

$(buildDir)/%.o: src/%.cpp
	mkdir -p $(buildDir)
	g++ $(flags) $< -c -o $@

run:
	./$(gameName)

clean:
	rm -rf $(buildDir) doc $(gameName)

.PHONY: doc
doc:
	doxygen src/documentation